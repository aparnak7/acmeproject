package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class VendorSearchResultsPage extends ProjectMethods {

	public VendorSearchResultsPage() {
		//apply PageFactory
		PageFactory.initElements(driver, this); 
	}
	
	@FindBy(how = How.XPATH,using="//div/table/tbody/tr[2]/td[1]") WebElement eleResult;
	
	
	public VendorSearchResultsPage printVendorName() {
		getElementText(eleResult);
	    return this;
}
}
