package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class HomePage extends ProjectMethods{

	public HomePage() {
       PageFactory.initElements(driver, this);
	}

	@FindBy(how = How.XPATH,using="//div/button[text()=\" Vendors\"]") WebElement eleVendors;
	@FindBy(how = How.LINK_TEXT,using="Search for Vendor") WebElement eleSearchVendor;
	
	public VendorSearchPage clickSearchVendor() {
		performAction(eleVendors, eleSearchVendor);
		return new VendorSearchPage();
	}
	
}
















