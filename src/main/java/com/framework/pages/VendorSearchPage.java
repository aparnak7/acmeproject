package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class VendorSearchPage extends ProjectMethods {
	
	public VendorSearchPage() {
		//apply PageFactory
		PageFactory.initElements(driver, this); 
	}
	
	@FindBy(how = How.ID,using="vendorTaxID") WebElement eleTaxID;
	@FindBy(how = How.ID,using="buttonSearch") WebElement eleSearchBtn;
	
	public VendorSearchPage enterTaxID(String data) {
		clearAndType(eleTaxID, data);
	    return this;
}
	public VendorSearchResultsPage clickSearch() {
		 click(eleSearchBtn);
	    return new VendorSearchResultsPage();
}
}