package com.framework.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.framework.design.ProjectMethods;
import com.framework.pages.LoginPage;

public class TC002_SearchVendor extends ProjectMethods {
	@BeforeTest
	public void setData() {
		testCaseName="TC002_SearchVendor";
		testDescription="searching a vendor";
		testNodes="Leads";
		author="Gayathri";
		category="smoke";
		dataSheetName="TC002";
		
	}
   @Test(dataProvider="fetchData")
	public void SearchVendor(String username,String password,String data) {
		new LoginPage()
		.enterUsername(username)
		.enterPassword(password)
		.clickLogin()
		.clickSearchVendor()
		.enterTaxID(data)
		.clickSearch()
		.printVendorName();
		
	
}
}
